class PhasedPlasmaBolt : Actor {
	int user_ang1;
	int user_radius1;

	default {
		Radius 10;
		Height 10;
		Speed 150;
		Damage 24;
		DamageType "Plasma";
		Decal "Scorch";
		Projectile;
		+RANDOMIZE
		+DONTTHRUST
		renderstyle "Add";
		alpha 0.90;
		Scale 1.00;
		DeathSound "weapons/plasmax";
		SeeSound "None";
		Obituary "$OB_MPPLASMARIFLE";
	}

	States {
		Spawn:
			FX12 ABCD 1 BRIGHT A_SpawnItem("PlasmaFlare",0,0);
			Loop;

		Xdeath:
			TNT1 AAAA 0 A_CustomMissile ("BluePlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
			TNT1 AAAAA 0 A_CustomMissile ("SuperPlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
			TNT1 AAAAAAAAAAAAAAAAA 0 A_CustomMissile ("BluePlasmaParticle", 0, 0, random (0, 360), 2, random (0, 360));
			TNT1 B 1 A_Explode(8,40,0);
			TNT1 B 4;
			TNT2 AAAAAAAA 4 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160));
			Stop;

		Death:
			TNT1 B 1 A_Explode(8,40,0);
			TNT1 AAAA 0 A_CustomMissile ("BluePlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
			TNT1 AAAAA 0 A_CustomMissile ("SuperPlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
			TNT1 AAAAAAAAAAAAAAAAA 0 A_CustomMissile ("BluePlasmaParticle", 0, 0, random (0, 360), 2, random (0, 360));
			TNT1 B 4;
			TNT2 AAAAAAAAAAA 4 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160));
			Stop;
	}
}

class SuperPlasmaFire : Actor {
	default {
		Height 0;
		Radius 0;
		Mass 0;
		+Missile
		+NoBlockMap
		-NoGravity
		//+LowGravity
		+DontSplash
		+DoomBounce
		+FORCEXYBILLBOARD
		BounceFactor 0.2;
		Gravity 0.8;
		RenderStyle "Add";
		Scale 0.005;
		//Speed 2
		Speed 2;
		+NoGravity
		-DOOMBOUNCE
		RenderStyle "Add";
		Scale 0.7;
		Alpha 0.9;
	}
		
	States {
		Spawn:
		Death:
			FIR5 ABCDEFGHIJKLMNOP 1 Bright A_FadeOut(0.5);
			Stop;
	} 
}



// ACTOR PhasedPlasmaBolt: Plasma_ball
// {
// 	var int user_ang1;
// 	var int user_radius1;
// 	Radius 10
// 	Height 10
// 	Speed 100
// 	Damage (frandom(80,80))
//     DamageType Plasma
// 	Decal "Scorch"
// 	Projectile
// 	+RANDOMIZE
// 	//+ROLLSPRITE 
// 	//+ROLLCENTER 
// 	renderstyle ADD
// 	alpha 0.90
// 	Scale 1.00
// 	DeathSound "weapons/plasmax"
// 	//SeeSound "PLSM2"
//     SeeSound "None"
// 	Obituary "$OB_MPPLASMARIFLE"

// }