class PhaseRifleGrenadeMissile : Actor {
  default {
    Radius 4;
    Height 4;
    Speed 50;
    Damage 60;
    +MISSILE
    +BLOODSPLATTER
    Gravity 0.7;

    +SKYEXPLODE
    +BOUNCEONFLOORS
    +BOUNCEONWALLS
    +BOUNCEONCEILINGS
    +EXPLODEONWATER
    +THRUSPECIES
    Species "Marines";
    DamageType "Kick";
    BounceFactor 0.5;
    WallBounceFactor 0.35;
    SeeSound "Weapons/GrenadeBounce";
    DeathSound "none";
    Decal "Scorch";
    Obituary "$OB_MPROCKET";
    Scale 0.5;
	}

	States {
    Spawn:
      TNT1 A 0 A_JumpIf(waterlevel > 1, "SpawnUnderwater");
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 6, "Explode");
      TNT1 A 0 A_CheckFloor("Death");
      GBPJ AAABBBCCCDDD 1 A_CustomMissile ("RocketSmokeTrail52", 2, 0, random (70, 110), 2, random (0, 360));
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 6, "Explode");
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      GBPJ EEEFFFGGGHHH 1 A_CustomMissile ("RocketSmokeTrail52", 2, 0, random (70, 110), 2, random (0, 360));
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 6, "Explode");
      Loop;
	
    SpawnUnderwater:
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      GBPJ ABCD 3;
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 6, "Explode");
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      GBPJ EFGH 3;
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 6, "Explode");
      Goto Spawn;
	
    Death:
      TNT1 A 0;
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 6, "Explode");
      GBPJ AAAA 3 A_CustomMissile ("RocketSmokeTrail52", 2, 0, random (70, 110), 2, random (0, 360));
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      Loop;

    XDeath:
      TNT1 A 0 A_Recoil(1);
      TNT1 A 0 A_SetAngle(random (90,-90) + angle);
      GBPJ C 1 ThrustThingZ(0,20,0,1);
      Goto Spawn;
	
    Explode:
    Death.Slime:
    Death.Fire:
    Death.Bullet:
    Death.Shotgun:
    Death.SSG:
    Death.Cutless:
    Death.Shrapnel:
      TNT1 A 0;
      TNT1 A 0 A_SpawnItemEx ("GrenadeExplosion",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 A 1 A_SpawnItemEx ("Footstep91",0,0, 40,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 A 1;
      TNT1 A 0 A_ChangeFlag("SHOOTABLE", 0);
      TNT1 A 0 Radius_Quake (3, 8, 0, 15, 0); // (intensity, duration, damrad, tremrad, tid)
      TNT1 A 0 A_SpawnItemEx ("ExplosionSplashSpawner", 0, 0, -20);
      TNT1 A 0 A_SpawnItemEx ("DetectFloorCrater",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 A 0 A_SpawnItemEx ("DetectCeilCrater",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 A 0 A_SpawnItemEx ("UnderwaterExplosion",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 A 0 A_SpawnItemEx ("ExplosionFlareSpawner",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 AAAAAAAAAA 0 A_CustomMissile ("BDExplosionparticlesBig", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAA 0 A_CustomMissile ("BDExplosionparticles2", 0, 0, random (0, 360), 2, random (0, 90));
      TNT1 AAAA 0 A_CustomMissile ("MediumExplosionFlames", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AA 0;
      TNT1 A 0 A_PlaySound("Explosion", 1);
      TNT1 A 0;
      TNT1 A 0 A_PlaySound("FAREXPL", 3);
      Stop;
	}
}

class PhaseRifleGrenade : PhaseRifleGrenadeMissile {
  default {
    -BOUNCEONFLOORS
    -BOUNCEONWALLS
    -BOUNCEONCEILINGS
    +SLIDESONWALLS
    Damage 150;
    SeeSound "None";
    Damagetype "Explosive";
  }

  States {
    Spawn:
      TNT1 A 0 A_JumpIf(waterlevel > 1, "SpawnUnderwater");
      GBPJ AAAAAAAAABBBBBBBCCCCCDDDDDEEEEEFFFFFGGGGGHHHHH 1 A_CustomMissile ("RocketSmokeTrail52", 2, 0, random (70, 110), 2, random (0, 360));
      Loop;

    SpawnUnderwater:
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      GBPJ ABCD 3;
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 1, "Explode");
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      GBPJ EFGH 3;
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 1, "Explode");
      Goto Spawn;

    Death:
    XDeath:
      TNT1 A 1;
      Goto Explode;
  }
}

class PhaseRifleHyperGrenade : PhaseRifleGrenadeMissile {
  default {
    -BOUNCEONFLOORS
    -BOUNCEONWALLS
    -BOUNCEONCEILINGS
    +SLIDESONWALLS
    Damage 150;
    SeeSound "None";
    Damagetype "Plasma";
  }

  States {
    Spawn:
      TNT1 A 0 A_JumpIf(waterlevel > 1, "SpawnUnderwater");
      GBPJ AAAAAAAAABBBBBBBCCCCCDDDDDEEEEEFFFFFGGGGGHHHHH 1 A_CustomMissile ("RocketSmokeTrail52", 2, 0, random (70, 110), 2, random (0, 360));
      Loop;

    SpawnUnderwater:
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      GBPJ ABCD 3;
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 1, "Explode");
      TNT1 A 0 A_GiveInventory("STGrenadeTimer", 1);
      GBPJ EFGH 3;
      TNT1 A 0 A_JumpIfInventory("STGrenadeTimer", 1, "Explode");
      Goto Spawn;

    Death:
    XDeath:
      TNT1 A 1;
      Goto Explode;

    Explode:
      TNT1 A 0;
      TNT1 A 0 Radius_Quake(4,34,0,12,0);
      TNT1 A 0 Radius_Quake(1,34,0,32,0);
      TNT1 A 0 A_SpawnItemEx ("ExplosionSplashSpawner", 0, 0, -20);
      TNT1 A 0 A_SpawnItemEx ("UnderwaterExplosion",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 AA 0 A_SpawnItemEx ("DetectFloorCrater",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 A 0 A_SpawnItemEx ("DetectCeilCrater",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION,0);
      TNT1 A 0 A_PlaySOund("BFGEXPL2", 1);
      TNT1 A 0 Bright A_SpawnItem("PhaseRifleHyperGrenadeShockWave",0,0,0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", 0, 0, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", 50, 0, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", -50, 0, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", 0, 50, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", 0, -50, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", 50, 50, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", 50, -50, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", -50, 50, 0);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeFlare", -50, -50, 0);
      TNT1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 0 A_CustomMissile ("PhaseRifleHyperGrenadeParticleFast", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 0 A_CustomMissile ("PhaseRifleHyperGrenadeParticleFaster", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 0 A_CustomMissile ("PhaseRifleHyperGrenadeParticleFast", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 0 A_CustomMissile ("PhaseRifleHyperGrenadeParticleFast", 0, 0, random (0, 360), 2, random (0, 360));

      TNT1 A 0 A_Explode(800, 800, 0);
      TNT1 A 0 Radius_Quake (3, 8, 0, 15, 0); 
      PRGB A 2 BRIGHT;
      TNT1 A 0 Bright A_BFGSpray("PhaseRifleHyperGrenadeSfxExtra", 80);
      TNT1 A 0 A_SpawnItemEx("PhaseRifleHyperGrenadeSfxExtra2", 0, 0, 0);
      TNT1 A 12;
      Stop;
  }
}
