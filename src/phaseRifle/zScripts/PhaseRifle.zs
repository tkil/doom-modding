const AMMO_CELL_NAME = "AmmoCell";
const AMMO_CLIP2_NAME = "Clip2";
const AMMO_ROCKET_NAME = "AmmoRocket";
const AMMO_SHELL_NAME = "AmmoShell";

const MAG_BURST_NAME = "MagPhaseRifleBurst";
const MAG_CELL_NAME = "MagPhaseRifleCell";
const MAG_GRENADE_NAME = "MagPhaseRifleGrenade";
const MAG_RAIL_NAME = "MagPhaseRifleRail";
const MAG_SHELL_NAME = "MagPhaseRifleShell";

const COOLDOWN_SHOTGUN = 15;
const COOLDOWN_RIFLE = 30;
const COOLDOWN_GRENADE = 0;

class PhaseRifle : BrutalWeapon
{
	int heat;
	Property heat : heat;
	string lastFireType;
	Property lastFireType : lastFireType;
	bool reloadCancel;
	Property reloadCancel : reloadCancel;
	string soundName;
	Property soundName : soundName;

	Default {
		PhaseRifle.heat 0;
		PhaseRifle.lastFireType "";
		PhaseRifle.reloadCancel false;
		PhaseRifle.soundName "";
		Weapon.BobStyle "smooth";
		Weapon.BobRangeX 0.25;
		Weapon.BobRangeY 0.25;
		// Weapon.BobSpeed 0.1;
		Weapon.SelectionOrder 10;
		Weapon.AmmoUse1 0;
		Weapon.AmmoGive1 0;
		Weapon.AmmoUse2 0;
		Weapon.AmmoGive2 0;
		Scale 0.9;
		Weapon.Kickback 100;

		Inventory.PickupSound "PLSDRAW";
		Weapon.AmmoType1 "";
		Weapon.AmmoType2 "";
		+WEAPON.NOAUTOAIM;
		+FORCEXYBILLBOARD;
		Tag "Phase Rifle";
		Inventory.PickupMessage "You got the Plasma Blaster! (Slot 6)";
	}

	action state CheckReloadCancel() {
		if (
			FindInventory("FiredPrimary")
				|| FindInventory("FiredSecondary")
				|| FindInventory("FiredWeaponState1")
				|| FindInventory("FiredWeaponState2")
				|| FindInventory("FiredWeaponState3")
				|| FindInventory("FiredWeaponState4")
				|| FindInventory("Reloading")
			) {
			invoker.reloadCancel = true;
		}
		return null;
	}

	action state CoolWeaponHeat() {
		if(invoker.heat > 0) {
			invoker.heat--;
		}
		return null;
	}

	action state CheckHyper() { 
		if(FindInventory("FiredWeaponState4")) {
			A_Giveinventory("HasPlasmaWeapon", 1);                      //otherwise don't do anything
		} else {
			A_Takeinventory("HasPlasmaWeapon", 1);
		}
		return null;
	}

	action state CheckAimDownSights() { 
		if (FindInventory("FiredSecondary") && !FindInventory("Zoomed")) {
			A_Giveinventory("Zoomed",1);
			A_ZoomFactor(2.0);
			return ResolveState("ZoomStart");                   //otherwise don't do anything
		} else if (!FindInventory("FiredSecondary") && FindInventory("Zoomed")) {
			A_Takeinventory("Zoomed",1);
			A_ZoomFactor(1.0);
			return ResolveState("ZoomStop");
		}
		return null;
	}

	action state ReloadShotgunSequence() {
		if (
			!FindInventory(AMMO_SHELL_NAME)
			|| FindInventory(AMMO_SHELL_NAME).amount == 0
			|| (
				FindInventory(MAG_SHELL_NAME)
				&& FindInventory(MAG_SHELL_NAME).amount == FindInventory(MAG_SHELL_NAME).maxAmount
			)
		) {
			return null;  
		} else {
			return ResolveState("ReloadAnimationShotgun");  
		}
	}

	action state ReloadTransferSequence(string source, string target, int exchangeAmmount, StateLabel reloadState, string soundName) {
		A_Takeinventory("Zoomed", 1);
		A_ZoomFactor(1.0);
		if (
			!FindInventory(source)
			|| FindInventory(source).amount == 0
			|| (
				FindInventory(target)
				&& FindInventory(target).amount == FindInventory(target).maxAmount
			)
		) {
			return null;  
		} else {
			while (
				(FindInventory(source) && !FindInventory(target))
				|| (
					FindInventory(target).amount < FindInventory(target).maxAmount
					&& FindInventory(source).amount > 0
				)
			) {
				A_Giveinventory(target, 1);
				A_Takeinventory(source, exchangeAmmount);
			}
			invoker.soundName = soundName;
			return ResolveState(reloadState);  
		}
	}

	action state ManageHeatCycle(int addedHeat, string lastFireType) {
		if(invoker.lastFireType != lastFireType) {
			invoker.heat = 0;
		}
		invoker.lastFireType = lastFireType;
		if (invoker.heat > 0) {
			return ResolveState("Ready");
		}
		invoker.heat += addedHeat;
		return null;
	}



	States {
		KickingFlash:
			PLSG EFGHIIIIIIIIHGFE 1; //16
			stop;
		
		AirKickingFlash:
			PLSG EFGHIIIIIIIIIIHGFE 1; //18
			stop;
		
		SlideKickingStart:
			PLSG EFGHI 1;
			PLSG II 17; //39 total
			Stop;

		SlideKickingEnd:
			PLSG IIIIIIHGFE 1; //10
			Stop;
			
		PuristGun:
			Goto Ready;
	
		Ready3:
		Ready:
			TNT1 A 0 CoolWeaponHeat();
			TNT1 A 0 A_JumpIfInventory("Zoomed",1,"ReadyNormalScope");
			TNT1 A 0 A_JumpIfInventory("HasPlasmaWeapon",1,"ReadyHyper");
			TNT1 A 0 A_WeaponReady(WRF_ALLOWRELOAD);
			Goto ReadyNormal;

		ReadyNormal:
			PRBN A 1;
			TNT1 A 0 {
				A_JumpIfInventory("IsRunning",1,"CheckSprint2");
				A_TakeInventory("UsedStamina", 2);
			}
			Goto ReadyLoop;

		ReadyNormalScope:
			PRZN A 1;
			TNT1 A 0 {
				A_JumpIfInventory("IsRunning",1,"CheckSprint2");
				A_TakeInventory("UsedStamina", 2);
			}
			Goto ReadyLoop;

		ReadyHyper:
			PRHN A 1;
			TNT1 A 0 {
				A_JumpIfInventory("IsRunning", 1, "CheckSprint2");
				A_TakeInventory("UsedStamina", 2);
			}
			Goto ReadyLoop;

		ReadyHyperScope:
			PRHN A 1;
			TNT1 A 0 {
				A_JumpIfInventory("IsRunning",1,"CheckSprint2");
				A_TakeInventory("UsedStamina", 2);
			}
			Goto ReadyLoop;

		ReadyLoop:
			TNT1 A 0 CheckHyper();
			TNT1 A 0 CheckAimDownSights();
			TNT1 A 0 A_JumpIfInventory("Kicking", 1, "DoKick");
			TNT1 A 0 A_JumpIfInventory("Taunting", 1, "Taunt");
			TNT1 A 0 A_JumpIfInventory("Salute1", 1, "Salute");
			TNT1 A 0 A_JumpIfInventory("Salute2", 1, "Salute");
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("TossGrenade", 1, "TossGrenade");
			TNT1 A 0 A_JumpIfInventory("IsRunning", 1, "CheckSprint");
			TNT1 A 0 A_JumpIfInventory("Unloading", 1, "Unload");
			TNT1 A 0 A_JumpIfInventory("FiredPrimary", 1, "Fire");
			TNT1 A 0 A_JumpIfInventory("FiredWeaponState1", 1, "FireWeaponState1");
			TNT1 A 0 A_JumpIfInventory("FiredWeaponState2", 1, "FireWeaponState2");
			TNT1 A 0 A_JumpIfInventory("FiredWeaponState3", 1, "FireWeaponState3");
			Goto Ready;

		CheckSprint:
			PLSG A 1 A_WeaponReady(WRF_NOFIRE);
			PLSN A 0 A_JumpIfInventory("IsStandingStill", 1, "Ready");
			PLSN A 0 A_JumpIfInventory("IsTacticalClass", 1, "StartSprint");
			Goto Ready;
			
		StartSprint:
			PLSG A 1 A_WeaponReady(WRF_NOFIRE);
			PLSN A 0 A_Takeinventory("Zoomed",1);
			PLSN A 0 A_ZoomFactor(1.0);
			PLSN A 0 A_JumpIfInventory("UsedStamina", 40, "StopSprintTired");
			
		Sprinting:	
			PLSG A 0 offset(-9,32);
			PLSN A 0 offset(-9,32) A_JumpIfInventory("IsStandingStill", 1, "Ready");
			PLSN A 0 offset(-9,32) A_JumpIfInventory("UsedStamina", 100, "StopSprintTired");
			PLAY A 0 offset(-9,32) ACS_ExecuteAlways(852, 0, 0, 0, 0); //Makes player faster.
			PLSN A 0 offset(-9,32) A_JumpIfInventory("PowerStrength", 1, 2);
			PLSN A 0 offset(-9,32) A_GiveInventory("UsedStamina", 4);
			PLSN A 0 offset(-9,32);
			PLSG A 1 offset(-9,34) A_SetPitch(pitch -0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(-6,36) A_SetPitch(pitch -0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(-3,38) A_SetPitch(pitch -0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(0,38) A_SetPitch(pitch -0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(3,36) A_SetPitch(pitch -0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(6,34) A_SetPitch(pitch -0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(9,32) A_SetPitch(pitch -0.5, SPF_INTERPOLATE);
			PLSN A 0 offset(9,32) A_WeaponReady(WRF_NOBOB);
			PLSN A 0 A_JumpIfInventory("Kicking",1,"DoKick");
			PLSN A 0 A_JumpIfInventory("Taunting",1,"Taunt");
			PLSN A 0 A_JumpIfInventory("Reloading",1,"Reload");
			PLSN A 0 offset(-9,32) A_GiveInventory("UsedStamina", 4);
			PLSN A 0 offset(9,33) A_SpawnItemEx("FootStep", 0, 0, 2, 0, 0, -4);
			PLSG A 1 offset(9,34) A_SetPitch(pitch +0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(6,36) A_SetPitch(pitch +0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(3,38) A_SetPitch(pitch +0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(0,38) A_SetPitch(pitch +0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(-3,36) A_SetPitch(pitch +0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(-6,34) A_SetPitch(pitch +0.5, SPF_INTERPOLATE);
			PLSG A 1 offset(-9,32) A_SetPitch(pitch +0.5, SPF_INTERPOLATE);
			PLSN A 0 offset(-9,32) A_WeaponReady(WRF_NOBOB);
			PLSN A 0 offset(-9,32) A_JumpIfInventory("IsRunning", 1, "Sprinting");
			Goto StopSprint;
			
		StopSprintTired:
			PLSG A 1;
			PLAY A 0 ACS_ExecuteAlways(853, 0, 0, 0, 0); //Makes player slower.
			PLSN A 0 A_PlaySound("Tired", 2);
			PLSN A 0 A_TakeInventory("UsedStamina", 2);
			PLSN A 5 A_WeaponReady;
			PLSN A 0 A_TakeInventory("UsedStamina", 2);
			PLSN A 5 A_WeaponReady;
			PLSN A 0 A_TakeInventory("UsedStamina", 2);
			PLSN A 5 A_WeaponReady;
			PLSN A 0 A_TakeInventory("UsedStamina", 2);
			PLSN A 5 A_WeaponReady;
			PLSN A 0 A_TakeInventory("UsedStamina", 2);
			PLSN A 5 A_WeaponReady;
			Goto Ready;
		StopSprint:
			PLSG A 1;
			PLSN A 0 A_JumpIfInventory("UsedStamina", 60, "StopSprintTired");
			PLAY A 0 ACS_ExecuteAlways(853, 0, 0, 0, 0); //Makes player slower.
			Goto Ready;
			
		Deselect:
			PLSN C 0;
			PLSN A 0 A_Takeinventory("Zoomed",1);
			PLSN A 0 A_ZoomFactor(1.0);
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleBurst", 1, 2);
			Goto DeselectNoAmmo;
			TNT1 A 0;
			PLSN A 0 A_Takeinventory("Reloading",1);
			PLSN A 0 A_Takeinventory("HasPlasmaWeapon",1);
			PLSN A 0 A_TakeInventory("TossGrenade", 1);
			PSGS ABCDE 1;
			TNT1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 0 A_Lower;
			TNT1 A 1 A_Lower;
			Wait;
		DeselectNoAmmo:
			PLSN A 0 A_Takeinventory("Reloading",1);
			PLSN A 0 A_Takeinventory("HasPlasmaWeapon",1);
			PLSN A 0 A_TakeInventory("TossGrenade", 1);
			PSGS FGHIJ 1;
			TNT1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 0 A_Lower;
			TNT1 A 1 A_Lower;
			Wait;
			
		Select:
			PLSN A 0;
			PLSN A 0 A_Takeinventory("StartDualWield",1);
			PLSN A 0 A_Giveinventory("GoSpecial",1);
			PLSN A 0 A_Takeinventory("FistsSelected",1);
			PLSN A 0 A_Takeinventory("SawSelected",1);
			PLSN A 0 A_Takeinventory("ShotgunSelected",1);
			PLSN A 0 A_Takeinventory("SSGSelected",1);
			PLSN A 0 A_Takeinventory("MinigunSelected",1);
			PLSN A 0 A_Giveinventory("PlasmaGunSelected",1);
			PLSN A 0 A_Takeinventory("RocketLauncherSelected",1);
			PLSN A 0 A_Takeinventory("GrenadeLauncherSelected",1);
			PLSN A 0 A_Takeinventory("BFGSelected",1);
			PLSN A 0 A_Takeinventory("BFG10kSelected",1);
			PLSN A 0 A_Takeinventory("RailGunSelected",1);
			PLSN A 0 A_Takeinventory("SubMachineGunSelected",1);
			PLSN A 0 A_Takeinventory("RevenantLauncherSelected",1);
			PLSN A 0 A_Takeinventory("LostSoulSelected",1);
			PLSN A 0 A_Takeinventory("FlameCannonSelected",1);
			PLSN A 0 A_Takeinventory("HasBarrel",1);
			PLSN A 0 A_TakeInventory("TossGrenade", 1);
			
			TNT1 A 1 A_Raise;
			TNT1 AAAAAAAAAAAAAA 0 A_Raise;
			PLSN A 0 A_GunFlash;
			PLSN A 0 A_JumpIfInventory("IsPlayingAsPurist", 1, "PuristGun");
			PLSN A 0 A_PlaySound("PLSDRAW");
		
		SelectAnimation:
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleBurst", 1, 2);
			Goto SelectNoAmmo;
			TNT1 A 0;
			PSGS EDCBA 1 A_JumpIfInventory("GoFatality", 1, "Steady");
			TNT1 A 0 A_Takeinventory("StartDualWield",1);
			TNT1 A 0 A_GunFlash;
			// TNT1 A 0 A_GiveInventory("BlasterPumpCounter",4);
			Goto Ready;
		SelectNoAmmo:
			TNT1 A 0;
			PSGS JIHGF 1 A_JumpIfInventory("GoFatality", 1, "Steady");
			TNT1 A 0 A_Takeinventory("StartDualWield",1);
			TNT1 A 0 A_GunFlash;
			// TNT1 A 0 A_GiveInventory("BlasterPumpCounter",4);
			Goto Ready;

		Fire:
			TNT1 A 0 {
				if (FindInventory("HasPlasmaWeapon") && FindInventory("Zoomed")) {
					return A_Jump(256, "FireHyperRail");
				} else if (FindInventory("Zoomed")) {
					return A_Jump(256, "FireRail");
				} else if (FindInventory("HasPlasmaWeapon")) {
					return A_Jump(256, "FireHyperBurst");
				} else {
					return A_Jump(256, "FireBurst");
				}
			}

		FireWeaponState1:
			TNT1 A 0 A_Takeinventory("FiredWeaponState1", 1);
			TNT1 A 0 A_JumpIfInventory("HasPlasmaWeapon", 1, "FireHyperShotgun");
			Goto FireShotgun;

		FireWeaponState2:
			TNT1 A 0 A_Takeinventory("FiredWeaponState2", 1);
			TNT1 A 0 A_JumpIfInventory("HasPlasmaWeapon", 1, "FireHyperGrenade");
			Goto FireGrenade;

		FireWeaponState3:
			TNT1 A 0 A_Takeinventory("FiredWeaponState3", 1);
			Goto AltReload;

		FireBurst:
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("FiredWeaponState4", 1, "FireHyperBurst");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleBurst", 1 , 1);
			Goto NoAmmo;
			TNT1 A 0 A_Takeinventory("MagPhaseRifleBurst", 1);
			TNT1 A 0 A_AlertMonsters;
			TNT1 A 0 BRIGHT A_FireBullets (1.25, 1.25, -1, 20, "HitPuff", FBF_NORANDOM);
			TNT1 A 0 A_FireProjectile("DecorativeTracer", random(-1.25,1.25), 0, 0, -12, 0, random(-1.25,1.25));
			TNT1 A 0 A_PlaySound("PRROUND", 1, 1.0);
			TNT1 A 0 A_FireCustomMissile("RifleCaseSpawn",1,0,8,0);
			RIFG A 0 A_JumpIfInventory("Zoomed",1,"FireZoomed");
			TNT1 A 0 A_ZoomFactor(0.98);
			PRBF B 1 BRIGHT;
			TNT1 A 0 A_SetPitch(Pitch-2.0, SPF_INTERPOLATE);
			TNT1 A 0 A_ZoomFactor(1.0);
			PRBF G 1 A_SetPitch(Pitch+2.0, SPF_INTERPOLATE);
			PRBN A 3;
			TNT1 A 0 A_Refire;
			PRBN A 2 A_WeaponReady(1);
			Goto Ready;

		FireShotgun:
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading",1,"Reload");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleShell", 1 , 1);
			Goto NoAmmo;
			PRBN A 1 ManageHeatCycle(COOLDOWN_SHOTGUN, "MagPhaseRifleShell");
			TNT1 A 0 A_Takeinventory("MagPhaseRifleShell", 1);
			TNT1 A 0 A_Takeinventory("Reloading",1);
			TNT1 A 0 A_JumpIfInventory("ShotgunWasEmpty", 1, "Pump2");
			TNT1 A 0 A_AlertMonsters;
			TNT1 A 0 A_PlaySound("weapons/sg", 1);
			TNT1 AAAAA 0 A_FireProjectile("DecorativeTracer", random(-2.5,2.5), 0, 0, -12, 0, random(-2.5,2.5));
			TNT1 A 0 A_SpawnItemEx("PlayerMuzzle1",30,0,45);
			TNT1 A 0 A_FireBullets (5, 1, 4, 13, "ShotgunPuff", FBF_NORANDOM);
			TNT1 A 0 A_FireBullets (2.5, 2.5, 5, 13, "ShotgunPuff2", FBF_NORANDOM);
			TNT1 A 0 A_FireCustomMissile("DistantFireSoundShotgun", random(-1,1), 0, 0, -12, 0, random(-1,1));
			TNT1 AAAAA 0 A_FireCustomMissile("DecorativeTracer", random(-5,5), 0, 0, 0, 0, random(-5,5));
			TNT1 A 0 A_SetPitch(-10.0 + pitch);
			PRBF A 1 BRIGHT A_SetPitch(+2.0 + pitch);
			PRBF A 1 BRIGHT;
			PRBN AAAA 1 A_SetPitch(+2.0 + pitch);
			Goto Ready;

	  FireRail:
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleRail", 1 , 1);
			Goto NoAmmo;
			PRZN A 1 ManageHeatCycle(COOLDOWN_RIFLE, "MagPhaseRifleRail");
			TNT1 A 0 A_Takeinventory("MagPhaseRifleRail", 1);
			TNT1 A 0 A_AlertMonsters;
			TNT1 A 0 A_SpawnItemEx("PlayerMuzzle1",30,0,45);
			TNT1 A 0 A_ZoomFactor(1.9);
			TNT1 A 0 A_AlertMonsters;
			TNT1 A 0 A_PlaySound("PRRAIL", 1, 1.0);
			TNT1 A 0 A_FireCustomMissile("ShakeYourAssMinor", 0, 0, 0, 0);
			TNT1 A 0 A_FireCustomMissile("YellowFlareSpawn",0,0,0,5);
			// Consider Sniper Puff
			PRZN A 1 BRIGHT A_RailAttack(210, 0, 0, "", "AA AA AA", RGF_SILENT|RGF_FULLBRIGHT|RGF_NORANDOMPUFFZ,0,"BulletPuff", 0, 0, 0, 0, 1.0, 1.0, "None", 2);
			TNT1 A 0 A_Takeinventory("RifleAmmo",1);
			TNT1 A 0 A_ZoomFactor(2.0);
			TNT1 A 0 A_FireCustomMissile("RifleCaseSpawn",1,0,8,0);
			PRZN A 2;
			PRZN A 2;
			// TNT1 A 0 A_JumpIfInventory("FiredSecondary", 1, 1); // Still Holding Aim
			PRZN A 10 A_WeaponReady(1);
			Goto Ready;
	
		FireGrenade:
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleGrenade", 1 , 1);
			Goto ReloadSecondaryGrenade;
			PRBN A 1 ManageHeatCycle(COOLDOWN_GRENADE, "MagPhaseRifleGrenade");
			TNT1 A 0 A_Takeinventory("MagPhaseRifleGrenade", 1);
			TNT1 A 0 A_AlertMonsters;
			// MGN1 A 0 A_JumpIfInventory("AmmoRocket", 1, 1)
			// Goto NoAmmo2
			// MGN1 A 0 A_JumpIfInventory("FiredMGGrenade", 1, "ReloadGrenade")
			TNT1 A 0 A_PlaySound("weapons/firegrenade");
			TNT1 A 0 ACS_NamedExecuteAlways("MGGrenadeCheck");
			TNT1 A 0 A_FireCustomMissile("RedFlareSpawn",-5,0,0,0);
			TNT1 AAAAAA 0 BRIGHT A_FireCustomMissile("ShotgunParticles", random(-16,16), 0, -1, random(-9,9));
			TNT1 A 0 BRIGHT A_FireCustomMissile("PhaseRifleGrenade", 0, 1, 0, 0);
			TNT1 A 0 A_SetPitch(-5.0 + pitch);
			TNT1 A 0 A_Recoil(8);
			PRBF AA 1 BRIGHT A_SetPitch(1.0 + pitch);
			PRBN AAA 1 A_SetPitch(1.0 + pitch);
			// MGN1 A 0 A_GiveInventory("FiredMGGrenade", 1)
			// PRBN A 10;
			TNT1 A 0 A_Refire;
			goto Ready;

		FireHyperBurst:
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleBurst", 1, 1);
			Goto NoAmmo;
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleCell", 1, 1);
			Goto NoAmmo;
			TNT1 A 0 A_Takeinventory("MagPhaseRifleBurst", 1);
			TNT1 A 0 A_Takeinventory("MagPhaseRifleCell", 1);
			TNT1 A 0 A_AlertMonsters;
			TNT1 A 0 A_PlaySound("PLSM2",CHAN_AUTO);
			TNT1 A 0 A_PlaySound("PLSM2",CHAN_AUTO);
			TNT1 A 0 A_ZoomFactor(0.99);
			TNT1 B 0 BRIGHT A_FireProjectile("PhasedPlasmaBolt", 0, 1, 0, -0);
			PRHF A 1 BRIGHT;
			TNT1 A 0 A_ZoomFactor(0.98);
			PRHF B 1 BRIGHT;
			//A_FireCustomMissile("AltHeavyPlasmaBall",0,1,0, 0)
			//TNT1 A 0 A_FireCustomMissile("BlueFlareSpawn",-5,0,0,0)
			PRHN A 1 BRIGHT A_ZoomFactor(1.0);
			PRHN A 1; //DEF 1
			Goto Ready;

		FireHyperShotgun:
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleShell", 1, 1);
			Goto NoAmmo;
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleCell", 8, 1);
			Goto NoAmmo;
			TNT1 A 0 A_Takeinventory("MagPhaseRifleShell", 1);
			TNT1 A 0 A_Takeinventory("MagPhaseRifleCell", 8);
			TNT1 A 0 A_AlertMonsters;
			// 
			TNT1 A 0 A_FireCustomMissile("Plasma_Ball", 0, 1, 0, -5);
			PLSN AAAAAA 0 A_FireCustomMissile("Blaster_Ball", random(-7,7), 0, 0, 0, 0, random(-2,2));
	    PLSN AAAA 0 A_FireCustomMissile("Blaster_Ball", random(-3,3), 0, 0, 0, 0, random(-1,1));
	    TNT1 A 0 A_FireCustomMissile("PlasmaFlareSpawn",-5,0,0,0);
	    PSGF A 1 A_PlaySound("PLSALTFR");
	    TNT1 A 0 A_ZoomFactor(0.9);
	    PSGF B 1 BRIGHT;
			TNT1 A 0 A_SetPitch(Pitch-2.5, SPF_INTERPOLATE);
			TNT1 A 0 A_ZoomFactor(1.0);
	    PSGF CDEFG 1 A_SetPitch(Pitch+0.5, SPF_INTERPOLATE);
	    PLSG D 3 A_WeaponReady(WRF_NOFIRE| WRF_NOBOB); //Allows quick switch
	    PSGF HIJKMNP 1;
	    PSGF Q 2;
			TNT1 A 0 A_FireCustomMissile("SmokeSpawner11",0,0,14,10);
	    SHTN R 0 A_PlaySound("weapons/sgpump", 3);
	    PSGF STUU 1;
			PSG2 UTSRQP 1;
	    PSG2 NMKJIH 1;
			Goto Ready;


	  FireHyperRail:
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleRail", 1, 1);
			Goto NoAmmo;
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleCell", 8, 1);
			Goto NoAmmo;
			TNT1 A 0 A_Takeinventory("MagPhaseRifleRail", 1);
			TNT1 A 0 A_Takeinventory("MagPhaseRifleCell", 8);
			TNT1 A 0 A_AlertMonsters;
			TNT1 A 0 A_FireCustomMissile("RailgunTrailEffectMissile", 0, 0, 0, 0);
			TNT1 A 0 A_FireCustomMissile("RailgunProjectile", 0, 1, 0, 0);
			TNT1 A 0 A_RailAttack(800, 0, 0, "", "66AAFF", RGF_FULLBRIGHT, 1, "RailgunPuff2");
			TNT1 A 0 A_PlaySound("RAILF1", 1);
			TNT1 A 0 A_SetPitch(-2.0 + pitch);
			TNT1 A 0 A_ZoomFactor(1.9);
			TNT1 A 0 A_JumpIfInventory("Zoomed",1,"ZoomedRecoil");
			PRZN A 1 BRIGHT A_AlertMonsters;
			TNT1 A 0 A_ZoomFactor(2.0);
			TNT1 A 0 A_SetPitch(2 + pitch);
			PRZN A 4 BRIGHT;
			// PRZN BABA 1 BRIGHT;

			// PRHF ABAB 1 BRIGHT;
			// PRHN AAAAAAA 1 A_SetPitch(0.0 + pitch);
			// TNT1 A 0 A_JumpIfInventory("RailgunAmmo",1,2)
			// Goto Reload
			TNT1 AA 0;
			PRZN AAAAA 2 A_WeaponReady(WRF_NOFIRE | WRF_NOBOB);
			TNT1 A 0 A_PlaySound("RAILR1", 2);
			PRZN AAAAAAAAAAAAAAAA 2 A_WeaponReady(WRF_NOFIRE | WRF_NOBOB);
			TNT1 A 0 A_PlaySound("BEPBEP", 2);
			TNT1 A 0 A_JumpIfInventory("FiredPrimary",1,"Fire");
			TNT1 A 0 A_Refire;
			TNT1 AAAA 0;
			Goto Ready;

		FireHyperGrenade:
			PRBN A 1 ManageHeatCycle(COOLDOWN_GRENADE, "MagPhaseRifleGrenade");
			TNT1 A 0 A_CheckReload;
			TNT1 A 0 A_JumpIfInventory("Reloading", 1, "Reload");
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleGrenade", 1, 1);
			Goto NoAmmo;
			TNT1 A 0 A_JumpIfInventory("MagPhaseRifleCell", 40, 1);
			Goto NoAmmo;
			TNT1 A 0 A_Takeinventory("MagPhaseRifleGrenade", 1);
			TNT1 A 0 A_Takeinventory("MagPhaseRifleCell", 40);
			TNT1 A 0 A_AlertMonsters;
			TNT1 A 0 A_PlaySound("weapons/firegrenade");
			TNT1 A 0 ACS_NamedExecuteAlways("MGGrenadeCheck");
			TNT1 A 0 A_FireCustomMissile("RedFlareSpawn",-5,0,0,0);
			TNT1 AAAAAA 0 BRIGHT A_FireCustomMissile("ShotgunParticles", random(-16,16), 0, -1, random(-9,9));
			TNT1 A 0 BRIGHT A_FireCustomMissile("PhaseRifleHyperGrenade", 0, 1, 0, 0);
			TNT1 A 0 A_SetPitch(-5.0 + pitch);
			TNT1 A 0 A_Recoil(8);
			PRHF BB 1 BRIGHT A_SetPitch(1.0 + pitch);
			PRHN AAA 1 A_SetPitch(1.0 + pitch);
			// MGN1 A 0 A_GiveInventory("FiredMGGrenade", 1)
			// PRBN A 10;
			TNT1 A 0 A_Refire;
			goto Ready;

		ZoomStart:
			// RIFZ ABC 1;
			PRBN AAA 1;
			Goto Ready;

		ZoomStop:
			PRBN AA 1;
			// RIFZ BA 1;
			Goto Ready;

		Spawn:
			TNT1 A 0;
			TNT1 A 0 A_ChangeFLag("THRUACTORS", 1);
			PLSG B 20;
			// PLSN A 0 A_SpawnItem("DropedPhaseRifle");
			Stop;
			
		NoAmmo:
			TNT1 A 0 A_PlaySound("weapons/empty", 4);
			PRBN A 15;
		Goto Ready;

		Reload:
			TNT1 A 0 {
				A_Takeinventory("Reloading", 1);
				if (FindInventory("Zoomed")) {
					return A_Jump(256, "ReloadPrimaryPriorityRail");
				} else if (FindInventory("HasPlasmaWeapon")) {
					return A_Jump(256, "ReloadPrimaryPriorityCell");
				} else {
					return A_Jump(256, "ReloadPrimaryNormal");

				}
			}
			Goto ReloadPrimaryPriorityNormal;
		
		AltReload:
			TNT1 A 0 {
				A_Takeinventory("Reloading", 1);
				if (FindInventory("Zoomed")) {
					return A_Jump(256, "ReloadPrimaryPriorityRail");
				} else if (FindInventory("HasPlasmaWeapon")) {
					return A_Jump(256, "ReloadSecondaryGrenade");
				} else {
					return A_Jump(256, "ReloadSecondaryNormal");
				}
			}
	

		ReloadPrimaryPriorityCell:
			TNT1 A 0 ReloadTransferSequence(AMMO_CELL_NAME, MAG_CELL_NAME, 1, "ReloadAnimationLong", "PLREADY");

		ReloadPrimaryPriorityNormal:
			TNT1 A 0 ReloadTransferSequence(AMMO_CLIP2_NAME, MAG_BURST_NAME, 1, "ReloadAnimationShort", "");
			TNT1 A 0 ReloadTransferSequence(AMMO_CLIP2_NAME, MAG_RAIL_NAME, 5, "ReloadAnimationLong", "PRRAILRL");
			TNT1 A 0 ReloadTransferSequence(AMMO_CELL_NAME, MAG_CELL_NAME, 1, "ReloadAnimationLong", "PLREADY");
			Goto Ready;

		ReloadPrimaryPriorityRail:
			TNT1 A 0 ReloadTransferSequence(AMMO_CLIP2_NAME, MAG_RAIL_NAME, 5, "ReloadAnimationLong", "PRRAILRL");
			TNT1 A 0 ReloadTransferSequence(AMMO_CLIP2_NAME, MAG_BURST_NAME, 1, "ReloadAnimationShort", "");
			TNT1 A 0 ReloadTransferSequence(AMMO_CELL_NAME, MAG_CELL_NAME, 1, "ReloadAnimationLong", "PLREADY");
			Goto Ready;

		ReloadSecondaryNormal:
			TNT1 A 0 ReloadShotgunSequence();
			TNT1 A 0 ReloadTransferSequence(AMMO_ROCKET_NAME, MAG_GRENADE_NAME, 1, "ReloadAnimationLong", "GRLLO2"); //GRLLO2
			Goto Ready;

		ReloadSecondaryGrenade:
			TNT1 A 0 ReloadTransferSequence(AMMO_ROCKET_NAME, MAG_GRENADE_NAME, 1, "ReloadAnimationLong", "GRLLO2"); //GRLLO2
			TNT1 A 0 ReloadShotgunSequence();
			Goto Ready;


		ReloadAnimationShort:
			TNT1 AAAA 0;
			// TNT1 A 0 A_PlaySound("PLREADY");
			TNT1 A 0 A_PlaySound("Reload", 6);
			TNT1 A 0 A_JumpIfInventory("TurboReload",1,"TurboReload");
			PSGS G 1;
			PSGR B 1;
			PLSN A 0 {
				A_FireCustomMissile("EmptyClipSpawn",-210,0,20,-20);
			}
			PSGR CDE 1;
			PSGR F 1 offset(-2,34);
			PSGR F 6 offset(0,32);
			PSGR GHIJ 1 offset(0,32);
			PSGR KKKKKK 1;
			PSGR LM 1;
			PSGR N 2;
			// PSGR LL 2;
			// PSGR LO 1;
			// PSGR PPPPPPP 1;
			// PSGR QR 1;
			// PSGR STU 1;
			// PSG2 P 1;
			// PSG2 QQ 1;
			// SHTN R 0 A_PlaySound("weapons/sgpump", 3);
			// PSG2 STUU 1;
			// PSG2 UTSRQP 1;
			PSG2 NMKJIH 1;
		Goto Ready;
	
		ReloadAnimationLong:
			TNT1 AAAA 0;
			// TNT1 A 0 A_PlaySound("PLREADY");
			TNT1 A 0 A_PlaySound("Reload", 6);
			TNT1 A 0 A_JumpIfInventory("TurboReload",1,"TurboReload");
			PSGS G 1;
			PSGR B 1;
			TNT1 A 0 {
				A_FireCustomMissile("EmptyClipSpawn",-210,0,20,-20);
			}
			PSGR CDE 1;
			PSGR F 1 offset(-2,34);
			PSGR F 6 offset(0,32);
			PSGR GHIJ 1 offset(0,32);
			PSGR KKKKKK 1;
			PSGR LM 1;
			PSGR N 2;
			PSGR LO 1;
			PSGR PPPPPPP 1;
			PSGR QR 1;
			// Pump
			PSGR STU 1;
			PSG2 P 1;
			PSG2 QQ 1;
			SHTN R 0 { A_PlaySound(invoker.soundName, 3); }
			PSG2 STUU 1;
			PSG2 UTSRQP 1;
			PSG2 NMKJIH 1;
		Goto Ready;
			
		ReloadAnimationShotgun:
			PSGS G 1;
			PSGR B 1;
			PSGR CDE 1;
			PSGR F 1 offset(-2, 34);
			PSGR F 6 offset(0, 32);

		ReloadAnimationShotgunInsertShell:
			TNT1 A 0 CheckReloadCancel();
			PSGR F 1;
			TNT1 A 0 CheckReloadCancel();
			PSGR F 1;
			TNT1 A 0 CheckReloadCancel();
			PSGR R 1;
			TNT1 A 0 CheckReloadCancel();
			PSGR N 1;
			TNT1 A 0 CheckReloadCancel();
			PSGR M 1;
			TNT1 A 0 CheckReloadCancel();
			PSGR L 1;
			TNT1 A 0 CheckReloadCancel();
			PSGR K 1;
			TNT1 A 0 CheckReloadCancel();
			PSGR J 1;
			TNT1 A 0 CheckReloadCancel();
			TNT1 A 0 {
				A_PlaySound("insertshell", 3);
				A_Giveinventory(MAG_SHELL_NAME, 1);
				A_Takeinventory(AMMO_SHELL_NAME, 1);
			}
			PSGR KLMNR 1; // Retract animation
			TNT1 A 0 CheckReloadCancel();
			// Check for stop
			TNT1 A 0 {
				if (
					!FindInventory(AMMO_SHELL_NAME)
					|| FindInventory(MAG_SHELL_NAME).amount == FindInventory(MAG_SHELL_NAME).maxAmount
					// || !FindInventory(AMMO_SHELL_NAME).amount == 0
					|| invoker.reloadCancel
					// || FindInventory(AMMO_SHELL_NAME).amount == 0
				) {
					invoker.reloadCancel = false;
					return A_Jump(256, "ReloadAnimationShotgunPump");
				} else {
					return A_Jump(256, "ReloadAnimationShotgunInsertShell");
				}
				// A_JumpIfInventory("Reloading", 1, "ReloadAnimationShotgunPump");
				// A_JumpIfInventory("FiredPrimary", 1, "ReloadAnimationShotgunPump");
				// A_JumpIfInventory("FiredSecondary", 1, "ReloadAnimationShotgunPump");
			}
			// Goto ReloadAnimationShotgunPump;

		ReloadAnimationShotgunPump:
			TNT1 A 0 A_Takeinventory("Reloading", 1);
			PSGR STU 1;
			PSG2 PQQ 1;
			TNT1 A 0 A_PlaySound("weapons/sgpump", 3);
			PSG2 STUU 1;
			PSG2 UTSRQP 1;
			PSG2 NMKJIH 1;
		Goto Ready;
			
		Unload:
			PLSN A 0 A_Takeinventory("Unloading",1);
			PLSN A 0 A_JumpIfInventory("AmmoCell",1,7);
			Goto Ready;
			TNT1 AAAAA 0;
			TNT1 AAAA 0;
			PLSN A 0 A_PlaySound("PLREADY");
			PSG2 A 1;
			PSGR B 1;
			PSG2 CDEFFFF 1;
			PSGR MLKKKKKK 1;
			PLSN A 0 A_PlaySound("BEPBEP");
			PSGR JIHGFFF 1;
			PSGR EDCBA 1;
			
		UnloadingSequence:
			PLSN A 0;
			PLSN A 0 A_Takeinventory("Unloading",1);
			PLSN A 0 A_JumpIfInventory("MagPhaseRifleBurst",1,3);
			Goto Ready;
			TNT1 AAAAAAA 0;
			PLSN A 0 A_Takeinventory("MagPhaseRifleBurst",1);
			PLSN A 0 A_Giveinventory("AmmoCell",1);
			Goto UnloadingSequence;
			
			
		DualWield:
			Goto Ready;
	}
}
