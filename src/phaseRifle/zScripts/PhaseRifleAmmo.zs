class MagPhaseRifleBurst : Ammo
{
    Default
    {
      Inventory.Amount 48;
      Inventory.MaxAmount 48;
    }
}

class MagPhaseRifleRail : Ammo
{
    Default
    {
      Inventory.Amount 4;
      Inventory.MaxAmount 4;
    }
}

class MagPhaseRifleShell : Ammo
{
    Default
    {
      Inventory.Amount 8;
      Inventory.MaxAmount 8;
    }
}

class MagPhaseRifleGrenade : Ammo
{
    Default
    {
      Inventory.Amount 1;
      Inventory.MaxAmount 1;
    }
}

class MagPhaseRifleCell : Ammo
{
    Default
    {
      Inventory.Amount 80;
      Inventory.MaxAmount 80;
    }
}


class InHyperMode : Inventory
{
    Default
    {
      Inventory.MaxAmount 1;
    }
}

// Actor MagPhaseRifleBurst : Ammo
// {
//    Inventory.Amount 48
//    Inventory.MaxAmount 48
//    Inventory.Icon "PLSGB0"
// }

// Actor BlasterPumpCounter : inventory
// {
//    Inventory.MaxAmount 4
// }

// Actor BlasterShootCounter : inventory
// {
//    Inventory.MaxAmount 4
// }


// Actor InHyperMode : inventory
// {
//    Inventory.MaxAmount 1
// }

