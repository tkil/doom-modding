class PhaseRifleHyperGrenadeFlare : Actor {
  default {
    +NOINTERACTION
    +NOGRAVITY
    +CLIENTSIDEONLY
    +NOTONAUTOMAP
    renderstyle "Add";
    radius 1;
    height 1;
    alpha 0.4;
    scale 0.4;
    alpha 0.1;
    yscale 1.6;
    xscale 1.6;
  }

  States {
    Spawn:
      LEYS BBBBBBBBBBBBBBBBBB 1 BRIGHT a_fADEIn(0.02);
      LEYS B 5 BRIGHT;
      LEYS BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB 1 BRIGHT a_fADEouT(0.04);
      Stop;
	}
}

class PhaseRifleHyperGrenadeParticle : Actor {
  default {
    Height 0;
    Radius 0;
    Mass 0;
    +Missile
    +NoBlockMap
    -NoGravity
    +LowGravity
    +DontSplash
    +DoomBounce
    BounceFactor 0.5;
    RenderStyle "Add";
    Scale 0.04;
  }

  States {
    Spawn:
    Death:
      SPKG A 2 Bright A_FadeOut(0.02);
      Loop;
    }
}

class PhaseRifleHyperGrenadeParticleFast : PhaseRifleHyperGrenadeParticle {
  default {
    Speed 10;
    Scale 0.1;
  }

  States {
    Spawn:
    Death:
    SPKB A 11 BRIGHT;
      SPKB AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 2 Bright A_FadeOut(0.06);
      Stop;
    }
}

class PhaseRifleHyperGrenadeParticleFaster : PhaseRifleHyperGrenadeParticleFast {
  default {
    Speed 20;
  }
}


class PhaseRifleHyperGrenadeSfxExtra : Actor {
  default {
    +NoBlockMap
    +NoGravity
    RenderStyle "Add";
    +FORCERADIUSDMG
    +THRUGHOST
    Alpha 0.75;
    Damage 2;
    Scale 0.6;
    Radius 1;
    Height 1;
    DamageType "Desintegrate";
  }
  States {
  Spawn:
    TNT1 A 0;
    TNT1 AAAAA 0 A_SpawnItemEx("PhaseRifleHyperGrenadeParticle", 0, 0, 14, (0.1)*Random(10, 40), 0, (0.1)*Random(-40, 40), Random(0, 360), 128);
    TNT1 AAAAA 0 A_SpawnItemEx("PhaseRifleHyperGrenadeParticle", 0, 0, 14, (0.1)*Random(10, 40), 0, (0.1)*Random(-40, 40), Random(0, 360), 128);
    TNT1 AAAAA 0 A_SpawnItemEx("PhaseRifleHyperGrenadeParticle", 0, 0, 14, (0.1)*Random(10, 40), 0, (0.1)*Random(-40, 40), Random(0, 360), 128);
    PRHG ABCDEFGHIJK 2 Bright A_SpawnItem("BlueFlareMedium",0,0);
    stop;
  }
}

class PhaseRifleHyperGrenadeSfxExtra2 : PhaseRifleHyperGrenadeSfxExtra {
  default {
    Scale 2.9;
  }
  States {
    Spawn:
      TNT1 A 0;
      // PRHG ABCDEFGH 2 Bright;
      // PRHG ABCDEFG 2 Bright;
      // PRHG ABCDEFGH 2 Bright;
      // SHWK ABCDEFGH 2 Bright;
      // PRHF BBBBBBBB 2 BRIGHT;
      PRHG ABCDEFGH 2 Bright;
      Stop;
    }
}

class PhaseRifleHyperGrenadeShockWave : Actor 
{ 
  default {
    Speed 0;
    Height 64;
    Radius 32;
    Scale 2.25;
    RenderStyle "add";
    Alpha 0.9;
    +DROPOFF 
    +NOBLOCKMAP 
    +NOGRAVITY 
    +CLIENTSIDEONLY
  }
  States { 
    Spawn: 
      SHWK A 1 BRIGHT;
      Goto Death;
    Death: 
      SHWK BCDEFGHIJKLMNOPQR 1 BRIGHT A_FadeOut(0.05);
      Stop;
    } 
} 