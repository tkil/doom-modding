class Plasma_Ball: FastProjectile
{
  default {
    Radius 10;
    Height 2;
    Speed 60;
    Damage 40;
    DamageType "Plasma";
    Decal "SmallerScorch";
    Projectile;
    +RANDOMIZE
    +SPECTRAL
    -NOBLOCKMAP
    +NOBLOOD
    +NORADIUSDMG
    +THRUSPECIES
    +MTHRUSPECIES
    +FORCEXYBILLBOARD
    Species "Marines";
    Health 5;
    Scale 1.0;
    renderstyle "ADD";
    alpha 0.99;
    DeathSound "weapons/plasmax";
    //SeeSound "PLSM9"
    SeeSound "None";
    Obituary "$OB_MPPLASMARIFLE";
  }

	States {

    Spawn:
      PLSS ABAB 1 Bright A_SpawnItem("BlueFlareSmall");
      Loop;
    Death:
      PLSN A 0;
      PLSN A 0 A_CustomMissile ("BluePlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAA 0 A_CustomMissile ("BluePlasmaParticle", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 B 1; //A_Explode(5,50,0)
      PLSE ABC 2 BRIGHT A_SpawnItem("BlueFlare");
      PLSE DE 2 Bright A_SpawnItem("BlueFlareSmall");
      TNT2 AAA 9 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160));
      Stop;
    
    XDeath:
      PLSN A 0;
      PLSN A 0 A_CustomMissile ("BluePlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAA 0 A_CustomMissile ("BluePlasmaParticle", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 B 1 A_Explode(5,50,0);
      PLSE ABC 2 BRIGHT A_SpawnItem("BlueFlare");
      PLSE DE 2 Bright A_SpawnItem("BlueFlareSmall");
      Stop;
  }
}





class Blaster_Ball: Plasma_Ball {
  default {
    Scale 0.5;
    Damage 20;
    Decal "BlasterScorch";
    Speed 75;
  }
  States {	
    Spawn:
      PLSS ABAB 1 Bright A_SpawnItem("BlueFlareSmall");
      Loop;
    Death:
      PLSN A 0 A_CustomMissile ("BluePlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAA 0 A_CustomMissile ("BluePlasmaParticle", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 B 1; //A_Explode(5,50,0);
      PLSE ABC 2 BRIGHT A_SpawnItem("BlueFlare");
      PLSE DE 2 Bright A_SpawnItem("BlueFlareSmall");
      TNT2 AAA 9 A_CustomMissile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160));
      Stop;
    
    XDeath:
      PLSN A 0;
      PLSN A 0 A_CustomMissile ("BluePlasmaFire", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 AAAAA 0 A_CustomMissile ("BluePlasmaParticle", 0, 0, random (0, 360), 2, random (0, 360));
      TNT1 B 1 A_Explode(5,50,0);
      PLSE ABC 2 BRIGHT A_SpawnItem("BlueFlare");
      PLSE DE 2 Bright A_SpawnItem("BlueFlareSmall");
      Stop;	
  }
}