//@ts-check
"use strict"

const { exec, spawn } = require("child_process")
const execAsync = require("util").promisify(exec)

/**
 * @param {string} command
 * @param {{
 *   isPanicOnError?: boolean
 *   output?: "none" | "string" | "terminal"
 * }} command
 * @returns {Promise<{
 *   stdout: string
 *   isError: boolean
 * }>}
 */
module.exports.cmd = async (command, { output = "terminal", isPanicOnError } = {}) => {
  if (output === "string") {
    const { stdout, stderr } = await execAsync(command)
    const isError = !Boolean(stdout) && Boolean(stderr)

    if (isPanicOnError && isError) {
      process.exit(1)
    } else {
      return {
        isError,
        stdout: stdout ?? stderr
      }
    }
  } else {
    return new Promise((res) => {
      const child = spawn(command, {
        shell: true,
        stdio: output === "terminal" ? "inherit" : "ignore"
      })

      child.on("exit", function (code) {
        const isError = code !== 0
        if (isPanicOnError && isError) {
          process.exit(1)
        } else {
          res({
            isError,
            stdout: ""
          })
        }
      })
    })
  }
}
