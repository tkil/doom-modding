//@ts-check
"use-strict"

const path = require("path")

const { stage } = require("./parts/stage")
const { compile } = require("./parts/compile")
const { loader } = require("./parts/loader")
const {
  FILE_PATH_DOOM_CONFIG,
  FILE_PATH_WAD_BRUTAL,
  FILE_PATH_WAD_DOOM_METAL,
  DIR_STAGING,
} = require("./parts/constants")

/** @param {string[]} devProjects */
const runStartWindows = (devProjects) =>
  loader({
    // isDryRun: true,
    config: FILE_PATH_DOOM_CONFIG,
    wads: [FILE_PATH_WAD_BRUTAL, FILE_PATH_WAD_DOOM_METAL],
    devWads: devProjects.map((p) => path.join(DIR_STAGING, p)),
    levelPack: "doom-wads/levels/doom-the-way-id-did/wad.wad",
  })

/** @param {string[]} devProjects */
const runStartMac = (devProjects) =>
  loader({
    isMac: true,
    // isDryRun: true,
    config: FILE_PATH_DOOM_CONFIG,
    wads: [FILE_PATH_WAD_BRUTAL],
    devWads: devProjects.map((p) => path.join(DIR_STAGING, p)),
  })

const start = async () => {
  const PROJECTS = ["phaseRifle"]

  for (const project of PROJECTS) {
    await compile(project)
    await stage(project)
  }

  // await runStartWindows(PROJECTS)
  await runStartMac(PROJECTS)
}
start()
