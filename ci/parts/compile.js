//@ts-check
"use-strict"

const path = require("path")

const { cp, readdir, mkdir, rm } = require("fs/promises")
const { cmd } = require("../utils/cmd");
const { ACC_EXE, ACC_DEBUG_FILE, DIR_SRC, DIR_DIST } = require("./constants");


/** @param {string} projectFolder */
module.exports.compile = async (projectFolder) => {
  const targetDir = path.join(DIR_SRC, projectFolder, "ACS")
  const outDir = path.join(DIR_DIST, projectFolder)

  await rm(DIR_DIST, { recursive: true, force: true })
  await mkdir(DIR_DIST, { recursive: true })
  await mkdir(path.join(DIR_DIST, projectFolder), { recursive: true })

  for(const fileName of await readdir(targetDir)) {
    if(/\.acs/.test(fileName)) {
      const targetFile = path.join(targetDir, fileName)
      await mkdir(path.join(outDir, "ACS"), { recursive: true })
      const outFile = path.join(outDir, "ACS", fileName.replace(".acs", ".o"))
      await cmd([
        ACC_EXE,
        `-d${ACC_DEBUG_FILE}`,
        targetFile,
        outFile
      ].join(" "))
    }
  }
}

  