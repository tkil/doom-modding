//@ts-check
"use-strict"

const path = require("path")


const JsZip = require("jszip")
const { cp, readFile, rm, writeFile } = require("fs/promises")
const { cmd } = require("../utils/cmd");
const { stage } = require("./stage");
const { compile } = require("./compile");
const { DIR_DIST, DIR_SRC } = require("./constants");

const zip = new JsZip();

const addFile = async (dir, zipDir, fileName) => {
    const content = await readFile(path.join(dir, fileName), "binary")
    zip.file(path.join(zipDir, fileName), content);
}


const writeZip = async (fileName) => {
    const data = await zip.generateAsync({ type: "nodebuffer" })
    await writeFile(fileName, data)
}

module.exports.pack = async () => {
    await addFile(DIR_DIST, "ACS", "SUPERMACHINEGUNEXTRAKEYS.o")

    // await addFile(DIR_SRC, "ACS", "SUPERMACHINEGUNEXTRAKEYS.o")
    await addFile(DIR_SRC, "", "DECORATE")
    await addFile(DIR_SRC, "", "KEYCONF")
    await addFile(DIR_SRC, "", "LOADACS")
    await addFile(DIR_SRC, "", "Machinegun.txt")

    await writeZip("SuperMachineGun.pk3")
    await writeZip("SuperMachineGun.zip")
}


