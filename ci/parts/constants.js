//@ts-check
"use-strict"

const path = require("path")

const DIR_ROOT = path.join(__dirname, "..", "..")

const DIR_ACC = path.join(DIR_ROOT, "acc")
const DIR_DIST = path.join(DIR_ROOT, "dist")
const DIR_SRC = path.join(DIR_ROOT, "src")
const DIR_LIB = path.join(DIR_ROOT, "lib")

const DIR_STAGING = path.join(DIR_ROOT, "staging")
const ACC_EXE = path.join(DIR_ACC, "acc.exe")
const ACC_DEBUG_FILE = path.join(DIR_ROOT, "acs.debug.err")

const FILE_PATH_DOOM_EXE = path.join(
  DIR_ROOT,
  "lib",
  "engine",
  "gzdoom-4-10-0-Windows-64bit",
  "gzdoom.exe"
)

const FILE_PATH_DOOM_APP = path.join(
  DIR_ROOT,
  "lib",
  "engine",
  "affinityPro",
  "affinityPro.app",
  "Contents",
  "MacOS",
  "gzdoom",
)

const FILE_PATH_DOOM_CONFIG = path.join(DIR_ROOT, "lib", "config", "config.ini")

const FILE_PATH_WAD_BRUTAL = path.join(
  DIR_ROOT,
  "lib",
  "engine",
  "brutalv21",
  "brutalv21.pk3"
)

const FILE_PATH_WAD_DOOM_1 = path.join(
  DIR_ROOT,
  "lib",
  "levels",
  "original",
  "Doom1.wad"
)

const FILE_PATH_WAD_DOOM_2 = path.join(
  DIR_ROOT,
  "lib",
  "levels",
  "original",
  "Doom2.wad"
)

const FILE_PATH_WAD_BG_COMP = path.join(
  DIR_ROOT,
  "lib",
  "levels",
  "bg-comp",
  "BGComp.wad"
)

const FILE_PATH_WAD_DOOM_METAL = path.join(
  DIR_ROOT,
  "lib",
  "music",
  "DoomMetalVol5",
  "DoomMetalVol5.wad"
)

const FILE_PATH_WAD_DOOM_HUD = path.join(
  DIR_ROOT,
  "lib",
  "ui",
  "HXRTCHUD_FIXED.1",
  "HXRTCHUD_FIXED_BDV21.pk3"
)

module.exports = {
  DIR_ROOT,
  DIR_ACC,
  DIR_DIST,
  DIR_SRC,
  DIR_STAGING,
  FILE_PATH_DOOM_CONFIG,
  FILE_PATH_DOOM_APP,
  FILE_PATH_DOOM_EXE,
  FILE_PATH_WAD_BRUTAL,
  FILE_PATH_WAD_DOOM_1,
  FILE_PATH_WAD_DOOM_2,
  FILE_PATH_WAD_BG_COMP,
  FILE_PATH_WAD_DOOM_METAL,
  FILE_PATH_WAD_DOOM_HUD,
  ACC_EXE,
  ACC_DEBUG_FILE,
}
