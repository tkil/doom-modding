//@ts-check
"use-strict"

const { cmd } = require("../utils/cmd")
const {
  FILE_PATH_DOOM_APP,
  FILE_PATH_DOOM_EXE,
  FILE_PATH_WAD_DOOM_1,
  FILE_PATH_WAD_DOOM_2,
} = require("./constants")

/**
 * @param {{
 *   isDryRun?: boolean
 *   isMac?: boolean
 *   iWad?: "DOOM_1" | "DOOM_2"
 *   wads: string[]
 *   devWads: string[]
 *   levelPack?: string
 *   config: string
 * }} options
 */
module.exports.loader = async ({
  config,
  devWads,
  isDryRun,
  isMac,
  iWad,
  levelPack,
  wads,
}) => {
  const statement = [
    isMac ? FILE_PATH_DOOM_APP : FILE_PATH_DOOM_EXE,
    ["-iwad", iWad === "DOOM_2" ? FILE_PATH_WAD_DOOM_2 : FILE_PATH_WAD_DOOM_1],
    ...wads.map((w) => ["-file", w]),
    levelPack ? ["-file", levelPack] : [],
    ...devWads.map((w) => ["-file", w]),
    config ? ["-config", config] : [],
  ].flat()
  console.log(
    [
      "",
      "=== Loading Doom ===",
      "",
      JSON.stringify(statement, null, 2),
      "",
      "====================",
      "",
    ].join("\n")
  )
  if(!isDryRun) {
    await cmd(statement.join(" "))
  }
}
