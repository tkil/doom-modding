//@ts-check
"use-strict"

const path = require("path")
const { DIR_STAGING, DIR_SRC, DIR_DIST } = require("./constants")
const { cp, mkdir, rm } = require("fs/promises")

/** @param {string} projectFolder */
module.exports.stage = async (projectFolder) => {
  await rm(DIR_STAGING, { force: true, recursive: true })

  await cp(
    path.join(DIR_DIST, projectFolder),
    path.join(DIR_STAGING, projectFolder),
    { recursive: true }
  )
  await cp(
    path.join(DIR_SRC, projectFolder),
    path.join(DIR_STAGING, projectFolder),
    { recursive: true }
  )
}
