//@ts-check
"use-strict"

const { stage } = require("./parts/stage");
const { compile } = require("./parts/compile");


const main = async () => {
  await compile()

  await stage()

}
main()
